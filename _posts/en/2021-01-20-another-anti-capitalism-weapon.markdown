---
layout: post
title: "Another Anti-Capitalism Weapon: Radical Anti-Hedonism (An Introduction)"
author: Paulo Elienay II
tags: ["radical anti-hedonism", "anti-hedonism", "hedonism", "philosophy"]
---

As humans, we are both cursed and blessed with the endless cycle of pursuing meaningless tokens marketed as practical solutions for much deeper problems, these problems being the ones created by the same machine that sells us the solution; Capitalism.

---
<br>

Hedonism is a philosophy that asserts seeking pleasure and avoiding suffering as the only possible ways to achieve happiness and comfort. This way of living paradoxically poisons the end goal, as pleasure can't be achieved by seeking pleasure.

<!-- absolute pleasure is not possible -->

A common man gets home, tired of work. He goes to the kitchen, grabs a beer, and watches pornography. This little amount of pleasure aids on his next workday, and the ones after that. The only reason the common man can drink his beer, which he sees as a gratification, is because of his work. The same for the device he used to watch pornography. And the internet that made the connection work. Maximum or absolute pleasure is not possible. Hedonists would contain themselves with little portions of pleasure, or a non-sustainable way of life, where one day the resources to achieve the objects of enjoyment will be too much to spend.

One could argue that the extra wealthy is 'immune' to this, in some way. But eventually, the hedonist cycle has to end, if a family of rich people continually practices this self-destructive non-sustainable way of life, it would eventually reach a generation where all the money was already spent, leaving nothing and thus ending the cycle.

<!-- pursue -->

But what if we achieved our end goal? A lot is discussed *how* to achieve dreams. How to get rich, how to talk to potential mates, how to find true love, how to get promoted, how to get that new job. Not a lot is talked about what happens *when* we achieve our end goal. Around 1876, the pessimist philosopher Mainländer started to ponder if his life was still worth living. He had just written his *magnum opus* and felt like he didn't have anything else to provide for humanity. If he acted right or not is beside the point of this article. Shortly after the publication of his book, Mainländer hanged himself. 

For existentialists, the rationalized 'meaning of life' shouldn't be an object. As much as we fantasize about our goals, we need for these same goals to be dreams. Or, at least, we need new dreams. Why do people get out of bed? Because they have something to do. And what if they didn't have something to do? Capitalism skillfully will never stop creating new imagery for dreams to be conceived. The new promotion, the new job, the new game, the *new*. These are our mouse running wheels. We need to keep running, keep moving. Paradoxically, again, the actual object of desire is never what the hedonist wants. He may dream about *that mansion* or *that woman*, but reaching his goal would mean losing his meaning in life. In such a state, a new goal needs to be obtained. Which breeds greed, and further distracts the subject from his lack of touch with his reality. Or fails miserably, leaving the person with nothing to hold on to, except the vast void of his meaningless existence.

<!-- capitalism -->

Realizing the systemic meatgrinder that controls your choices by using your judgment, -- "what is best for me?" -- one must inquire. What to do about this? If seeking what seems to be uncontestably the right decision is already tainted by the system, what can you do? Radical anti-hedonism is the renunciation of this current standardized model of living. It's not about removing everything that can be seen as pleasurable from your life but consciously making decisions to undermine the deep mental control that hedonists' practices have on the human being. It's also not interpassivity, the point is not to 'ironically' practice fruitless actions, to try to feel better about doing what the subject itself deems as morally wrong. As Mark Fisher writes in his book, Capitalist Realism: "[...] we are able to fetishize money in our actions only because we have already taken an ironic distance towards money in our heads". The goal of radical anti-hedonism is not to make one feel better about their own poor decisions, but as a way, a possibly not before considered way of judging how to proceed about capitalism. If capitalism is not going away, that leaves us with the need to protect the only thing that can still be protected from it: our minds.